import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EqualVadilatorDirective } from './shared module/directives/equal-vadilator.directive';
import { FormsModule } from '@angular/forms';
import { ValidatorComponent } from './modules/validator/validator.component';

@NgModule({
  declarations: [
    AppComponent,
    EqualVadilatorDirective,
    ValidatorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
