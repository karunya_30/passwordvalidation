import { Component, OnInit } from '@angular/core';
import { User } from '../../shared module/interface/user.interface';

@Component({
  selector: 'app-validator',
  templateUrl: './validator.component.html',
  styleUrls: ['./validator.component.css']
})
export class ValidatorComponent implements OnInit {

  title = 'app';
  public user: User;

  ngOnInit() {
    this.user = {
      password: '',
      confirmPassword: ''
    };
  }

  save(model: User, isValid: boolean) {
    console.log(model, isValid);
  }

}
