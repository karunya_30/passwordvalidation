import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[validateEqual][formControlName],[validateEqual][formControl],[validateEqual][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: forwardRef(() => EqualVadilatorDirective), multi: true }
  ]
})
export class EqualVadilatorDirective implements Validator {

  constructor(@Attribute('validateEqual') public validateEqual: string,
              @Attribute('reverse') public reverse: string) {

  }

  private get isReverse() {
    if (!this.reverse) { return false; }
    return this.reverse === 'true' ? true : false;
  }

  validate(c: AbstractControl): { [key: string]: any } {
    // self value
    const confirmPassword = c.value;
    console.log(c.value);
    console.log(c);
    console.log(AbstractControl);

    // control vlaue
    const Password = c.root.get(this.validateEqual);
    console.log(c.root);
    console.log(Password)
    // value not equal
    if (Password && confirmPassword !== Password.value && !this.isReverse) {
      return {
        validateEqual: false
      };
    }
    return null;
  }

}
